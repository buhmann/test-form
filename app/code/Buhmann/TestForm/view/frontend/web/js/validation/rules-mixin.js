/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'Magento_Ui/js/lib/validation/validator',
], function ($, validator) {
    "use strict";

    return function () {
        validator.addRule(
            'validate-phoneNumber',
            function(value) {
                return value.length === 0 || value.match(/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im);
            },
            $.mage.__('Please enter a valid phone.')
        );
    }
});