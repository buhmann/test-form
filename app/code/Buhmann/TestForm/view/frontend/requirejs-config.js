/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
var config = {
    config: {
        mixins: {
            'mage/validation': {
                'Buhmann_TestForm/js/validation/rules-mixin': true
            }
        }
    }
};