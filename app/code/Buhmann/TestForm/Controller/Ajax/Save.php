<?php
/**
 * Copyright © Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Buhmann\TestForm\Controller\Ajax;

use Magento\Framework\App\Action\Action;
use Magento\Framework\Controller\ResultFactory;

class Save extends Action
{
    public function execute()
    {
        $post = $this->getRequest()->getPost();
        $data = [];
        try {
            $this->messageManager->addSuccess(__('Test form has been saved.'));
        } catch (\Exception $e) {
            $this->messageManager->addError(__('Something went wrong.'));
        }

        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultJson->setData($data);

        return $resultJson;
    }
}
